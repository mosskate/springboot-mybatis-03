package com.wb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMybatis03Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootMybatis03Application.class, args);
	}

}
